# Portable Console

[![pipeline status](https://gitlab.com/reendux/portableconsole/badges/master/pipeline.svg)](https://gitlab.com/reendux/portableconsole/commits/master)

Portable and universal framework for in-game console for debugging and more.
Works with unity.

# Notes
Project is not using any reflection.

# Usage

    class Program : IConsoleInput
    {
        static void Main(string[] args)
        {
            ConsoleParser.Register(new Program()); //Registers class which implements IConsoleInput.
            Console.Write("Write command:");
            string str = Console.ReadLine();
            foreach (string line in AutoSuggestion.Check(str)) //Performs autosuggestion on command.
            {
                Console.Write(line + "\n");
            }
            str = Console.ReadLine();
            if (ConsoleParser.Command(str)) //Parsers command and executes it.
            {
                Console.WriteLine("Command found.");
            } else
            {
                Console.WriteLine("Command not found or wrong number of parameters.");
            }
            Console.ReadLine();
            foreach (string line in ConsoleOutput.GetOutput()) //Returns console output list
            {
                Console.Write(line + "\n");
            }
            Console.Read();
        }

        //Program.MultipleInput
        public static void MultipleInput()
        {
            ConsoleOutput.Add("test\ntest\ntest\n"); //adds info to console output
        }

        //Program.Print
        public static void Print(string str)
        {
            ConsoleOutput.Add(str); //adds info to console output
        }

        //Program.Numbers
        public static void Numbers(string a, string b)
        {
            ConsoleOutput.Add((int.Parse(a) + int.Parse(b)).ToString()); //adds info to console output
        }

        object IConsoleInput.RegisterClass()
        {
            return this;
        }

        List<string> IConsoleInput.RegisterInputs()
        {
            List<string> list = new List<string>
            {
                "MultipleInput",
                "Numbers",
                "Print"
            };
            return list;
        }

        void IConsoleInput.Invoke(int commandNumber, List<string> data)
        {
            switch (commandNumber) //the methods must match order in list returned by RegisterInputs()
            {
                case 0:
                    MultipleInput();
                    break;
                case 1:
                    Numbers(data[0], data[1]);
                    break;
                case 2:
                    Print(data[0]);
                    break;
            }
        }
    }
    
# License

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Licencja Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />This project is available under license <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International</a>.