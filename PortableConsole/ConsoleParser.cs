﻿using System.Collections.Generic;
using PortableConsole;

namespace PortableConsole
{
    public class ConsoleParser
    {
        internal static List<ConsoleInputObject> objects = new List<ConsoleInputObject>();

        /// <summary>
        /// Registers class name and methods to allow running from console input. 
        /// </summary>
        /// <param name="ci">Methods with class name to show in console.</param>
        public static void Register(IConsoleInput ci)
        {
            objects.Add(new ConsoleInputObject(ci.RegisterClass(), ci.RegisterInputs(), ci));
        }

        /// <summary>
        /// Command to run.
        /// </summary>
        /// <param name="command">Command to run.</param>
        /// <returns>Is command found and fits number of parameters.</returns>
        public static bool Command(string command)
        {
            string[] method = command.Split(' ');
            string[] data = method[0].Split('.');
            foreach (ConsoleInputObject obj in objects)
            {
                if (data[0] == obj.obj.GetType().Name && obj.inputs.Contains(data[1]))
                {
                    if (method.Length > 1)
                    {
                        List<string> parameters = new List<string>();
                        for (int i = 1; i < method.Length; i++)
                        {
                            parameters.Add(method[i]);
                        }
                        obj.ci.Invoke(obj.inputs.IndexOf(data[1]), parameters);
                        return true;
                    }
                    else
                    {
                        obj.ci.Invoke(obj.inputs.IndexOf(data[1]), null);
                        return true;
                    }
                }
            }
            return false;
        }
    }

    /// <summary>
    /// Interface with all data needed to properly run methods from console input.
    /// </summary>
    public interface IConsoleInput
    {
        /// <summary>
        /// Registers class object.
        /// </summary>
        /// <returns>Class object.</returns>
        object RegisterClass();

        /// <summary>
        /// Registers class method to run from console input.
        /// </summary>
        /// <returns>List of method names to run from console input.</returns>
        List<string> RegisterInputs();

        /// <summary>
        /// Invokes method directly.
        /// </summary>
        /// <param name="commandNumber">Position in returned list from RegisterInputs().</param>
        /// <param name="data">Method input.</param>
        void Invoke(int commandNumber, List<string> data);
    }
}

internal class ConsoleInputObject
{
    public object obj;
    public List<string> inputs;
    public IConsoleInput ci;

    public ConsoleInputObject(object obj, List<string> inputs, IConsoleInput ci)
    {
        this.obj = obj;
        this.inputs = inputs;
        this.ci = ci;
    }
}