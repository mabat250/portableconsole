﻿using System;
using System.Collections.Generic;

namespace PortableConsole
{
    public class AutoSuggestion
    {
        /// <summary>
        /// Checks if similar commands exists.
        /// </summary>
        /// <param name="command">Command to find similar.</param>
        /// <returns>List of commands in form: className.methodName numberOfParameters</returns>
        public static List<string> Check(string command)
        {
            if (command == "")
            {
                return new List<string> { "" };
            }
            List<string> autocomplete = new List<string>();

            string[] data = command.Split(' ');
            string[] names = data[0].Split('.');

            foreach (ConsoleInputObject obj in ConsoleParser.objects)
            {
                Type type = obj.obj.GetType();
                if (obj.obj.GetType().Name.Contains(names[0]))
                {
                    foreach (string str in obj.inputs)
                    {
                        autocomplete.Add(type.Name + '.' + str + " " + type.GetMethod(str).GetParameters().Length);
                    }
                }
            }

            return autocomplete;
        }
    }
}