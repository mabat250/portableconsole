﻿using System.Collections.Generic;

namespace PortableConsole
{

    public class ConsoleOutput
    {
        private static List<string> output = new List<string>();

        /// <summary>
        /// Adds text to console output.
        /// </summary>
        /// <param name="str"> Text to add in console output.</param>
        public static void Add(string str)
        {
            output.Add(str);
        }

        /// <summary>
        /// Returns console output.
        /// </summary>
        /// <returns>Array with output.</returns>
        public static List<string> GetOutput()
        {
            return output;
        }

        /// <summary>
        /// Clears console.
        /// </summary>
        public static void ClearConsole()
        {
            output.Clear();
        }
    }
}